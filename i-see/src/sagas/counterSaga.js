import {INCREASE, INCREASE_DONE} from "../actions/type";
import {delay} from 'redux-saga/effects';
import {takeLatest, put} from 'redux-saga/effects';
import {all} from 'redux-saga/effects'
function* incrementAsync() {
    yield delay(1000);
    yield put({ type: INCREASE_DONE });
}

export function* watchIncrementAsync() {
    yield takeLatest(INCREASE, incrementAsync);
}