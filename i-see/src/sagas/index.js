import {watchIncrementAsync} from './counterSaga';
import {watchFetchUser} from "./userSaga";
import {all} from 'redux-saga/effects'
export default function* rootSaga() {
    yield all( [
        watchIncrementAsync(),
        watchFetchUser()
    ])
}