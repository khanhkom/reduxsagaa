import React,{Component} from 'react';
import {View,Text,Container} from 'native-base';
import App from '../App/App'
import Main from '../Main'
class Home extends Component {
    render(){
        const region = {
            latitude:20.9826962,
            longitude:105.7879711,
            latitudeDelta:0.01,
            longitudeDelta:0.01,
        }
        return(
            <Container>
                <Main/>
            </Container>
        )
    }
}
export default Home