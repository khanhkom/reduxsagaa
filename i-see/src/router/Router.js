//import lib
import {  createStackNavigator,createAppContainer } from 'react-navigation';
import React from 'react';
import TestVoice from '../container/Home/VoiceTest'
import Details from '../container/Details/Details';
import Home from '../container/Home/Home';
console.disableYellowBox = true
const AppNavigator = createStackNavigator({
	Details: { screen: Details },
	Home: { screen: Home },
	TestVoice:{screen:TestVoice}
}, {
		initialRouteName: 'Home',
		swipeEnabled: true,
		animationEnabled: false,
		headerMode: 'none',
		navigationOptions: {
			header: null
		},
		lazy: true,
		cardStyle: {
			backgroundColor: '#FFF',
			opacity: 1
		},
	})
const Router=createAppContainer(AppNavigator);
export default Router;
