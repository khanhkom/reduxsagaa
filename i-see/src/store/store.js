import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import reducer from '../reducer/index';
import createSagaMiddleWare from 'redux-saga';
import rootSaga from '../sagas/index'
const sagaMiddleWare = createSagaMiddleWare()
const store = createStore(
    reducer,
    {},
    applyMiddleware(sagaMiddleWare)
);
sagaMiddleWare.run(rootSaga);
export default store;