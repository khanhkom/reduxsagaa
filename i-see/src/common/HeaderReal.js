import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image, TextInput, TouchableOpacity, StatusBar
} from 'react-native';

//import from Lib
import Ionicons from 'react-native-vector-icons/Ionicons';

//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/colors';

type Props = {
  title: string,
  onButtonLeft: Function,
  onButtonRight: Function,
  iconLeft: any,
  iconRight: any,
}
export default HeaderReal = (props: any) => {
  const { title, onButtonLeft, onButtonRight, iconLeft, iconRight } = props;
  return (
    <View style={styles.header}>
      <StatusBar backgroundColor={colors.colorPink} />
      <TouchableOpacity
        onPress={() => onButtonLeft()}
        style={styles.btnLeft}
      >
        <Ionicons name={iconLeft} size={26 * STYLES.heightScreen / 640} color="#fff" />
      </TouchableOpacity>
      <View style={styles.cntTextInput}>
        <Text style={[styles.text, { fontSize: STYLES.fontSizeLabel, fontWeight: 'bold' }]}>
          {title}
        </Text>
      </View>
      <TouchableOpacity
        style={styles.btnRight}
        onPress={() => onButtonRight()}
      >
         <Ionicons name={iconRight} size={26 * STYLES.heightScreen / 640} color="#fff" />
      </TouchableOpacity>
    </View>
  )
}
const styles = StyleSheet.create({
  header: {
    height: (43 * STYLES.heightScreen / 640),
    width: STYLES.widthScreen,
    flexDirection: 'row',
    backgroundColor: colors.colorPink,
    elevation: 4,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingRight: 5,

  },
  btnRight:{
    flex: 0,
    width: 55 * STYLES.heightScreen / 640,
    alignItems: 'flex-end',
    marginRight: 3 * STYLES.widthScreen / 360
  },
  btnLeft: {
    flex: 0,
    paddingLeft: 8,
    width: 55 * STYLES.heightScreen / 640,
  },
  logo: {
    width: 26 * STYLES.heightScreen / 640,
    height: STYLES.heightScreen * (35 / 640),
  },
  cntlogoStart: {
    width: STYLES.widthScreen * (35 / 360),
    height: STYLES.heightScreen * (35 / 640),
  },
  logoStart: {
    width: STYLES.widthScreen * (35 / 360),
    height: STYLES.heightScreen * (25 / 640),
  },
  text: {
    color: colors.colorWhite,
    fontSize: STYLES.fontSizeText,
    textAlign: "center",
    textAlignVertical: "center",
    fontFamily: 'Roboto-Medium'
  },
  textMini: {
    color: colors.colorWhite,
    fontSize: STYLES.fontSizeMiniText,
    textAlign: "center",
    textAlignVertical: "center",
    fontFamily: 'Roboto-Regular',
  },
  cntTextInput: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 0,
    //height: (30 * STYLES.heightScreen / 640),
  },
  textInput: {
    width: STYLES.widthScreen * 0.53,
    height: (30 * STYLES.heightScreen / 640),
    backgroundColor: colors.header,
    fontSize: STYLES.fontSizeNormalText,
    fontFamily: 'Roboto-Regular',
    marginLeft: 5,
    //textAlign:'center',
    paddingTop: 0, paddingBottom: 0,
    textAlignVertical: 'center',
    color: colors.white,
  }
})
