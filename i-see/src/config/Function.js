import { Dimensions } from "react-native";
const { width, height } = Dimensions.get('window');
export const responsiveHeight = (h) => {
	return height * (h / 100);
};
export const responsiveWidth = (w) => {
	return width * (w / 100);
};
export const calTime = (timeStart,timeEnd) => {
	let minusStart=timeStart.getTime();
	let minusEnd=timeEnd.getTime();
	let res=(minusEnd-minusStart)/1000;
	return Math.floor(res);
};
export const responsiveWidthComponent = (widthComponent, w) => {
	return widthComponent * (w / 100);
};

export const responsiveFontSize = (f) => {
	return Math.sqrt((height * height) + (width * width)) * (f / 100);
};

export const responsiveVar = (h, v) => {
	return v * (h / 100);
};

export const slitNumber = (str) => {
	let res = "";
	for (let i = 0; i < str.length; i++)
		if (str[i] != '.') res = res + " " + str[i];
	return res;
};